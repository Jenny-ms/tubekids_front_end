import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenService } from './../Services/token.service';
@Injectable({
  providedIn: 'root'
})
export class HttpVideosService {

  private url = 'http://localhost:8000/api/videos'

  constructor(
  	private http : HttpClient,
  	private token: TokenService
  ) { }

  get(){
  	return this.http.get(`${this.url}`, this.options());
  }
  getvideo(id){
    return this.http.get(`${this.url}/${id}`, this.options());
  }
  post(data){
  	return this.http.post(`${this.url}`, data, this.options());
  }
  put(data){
  	return this.http.post(`${this.url}/${data.id}`, data, this.options());
  }
  delete(id){
  	return this.http.delete(`${this.url}/${id}`, this.options());
  }

  options() {
  	return { headers: new HttpHeaders({'Authorization' : `Bearer ${this.token.get()}`})}
  }
}