import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { MenuComponent } from './components/menu/menu.component';
import { VideoMainComponent } from './components/videos/main/main.component';
import { VideoReadComponent } from './components/videos/read/read.component';
import { VideoUpdateComponent } from './components/videos/update/update.component';
import { VideoCreateComponent } from './components/videos/create/create.component';
import { ProfileMainComponent } from './components/profile/main/main.component';
import { ProfileReadComponent } from './components/profile/read/read.component';
import { ProfileUpdateComponent } from './components/profile/update/update.component';
import { ProfileCreateComponent } from './components/profile/create/create.component';
import { CodeComponent } from './components/code/code.component';

const routes: Routes = [
{
	path: 'login',
	component:LoginComponent
},
{
	path: 'signup',
	component:SignupComponent
},
{
	path: 'menu',
	component:MenuComponent
},
{
	path: 'videos',
	component:VideoMainComponent
},
{
	path: 'videos/read/:id',
	component:VideoReadComponent
},
{
	path: 'videos/create',
	component:VideoCreateComponent
},
{
	path: 'videos/update/:id',
	component:VideoUpdateComponent
},
{
	path: 'profile',
	component:ProfileMainComponent
},
{
	path: 'profile/read/:id',
	component:ProfileReadComponent
},
{
	path: 'profile/create',
	component:ProfileCreateComponent
},
{
	path: 'profile/update/:id',
	component:ProfileUpdateComponent
},
{
	path: 'code',
	component:CodeComponent
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
