import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { MenuComponent } from './components/menu/menu.component';

import { HttpClientModule } from '@angular/common/http';
import { CodeComponent } from './components/code/code.component';
import { VideoCreateComponent } from './components/videos/create/create.component';
import { VideoUpdateComponent } from './components/videos/update/update.component';
import { VideoReadComponent } from './components/videos/read/read.component';

import { ProfileCreateComponent } from './components/profile/create/create.component';
import { ProfileUpdateComponent } from './components/profile/update/update.component';
import { ProfileReadComponent } from './components/profile/read/read.component';
import { ProfileMainComponent } from './components/profile/main/main.component';
import { VideoMainComponent } from './components/videos/main/main.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    MenuComponent,
    CodeComponent,
    VideoCreateComponent,
    VideoUpdateComponent,
    VideoReadComponent,
    ProfileMainComponent,
    ProfileCreateComponent,
    ProfileUpdateComponent,
    ProfileReadComponent,
    VideoMainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
