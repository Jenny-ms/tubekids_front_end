import { Component, OnInit } from '@angular/core';
import { HttpProfilesService } from '../../../Services/http-profiles.service';
import { TokenService } from '../../../services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class ProfileCreateComponent implements OnInit {

  ngOnInit() {
  }
public form = {
	full_name  : null,
	user_name  : null,
	age        : null,
	pin        : null
};

public error = [];

  constructor(
  	private http: HttpProfilesService,
  	private Token : TokenService,
  	private router: Router
  	) { }


onSubmit(){
	this.http.post(this.form).subscribe(
		data => this.handleResponse(data),
		error => this.handleError(error)
	);
}

handleResponse(data){
	this.router.navigateByUrl('/profile');
} 
handleError(error) {
	this.error = error.error.errors;
}
}
