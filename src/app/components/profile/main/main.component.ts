import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpProfilesService } from '../../../Services/http-profiles.service';
import { ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class ProfileMainComponent implements OnInit {

 	public profiles : any[]
	private url : string;
  	constructor(private http : HttpProfilesService, private route : ActivatedRoute,
    private router : Router,) {
  		this.url = "profiles";
  	}

  	ngOnInit() {
  		this.getList();
  	}

  	getList() {
  		this.http.get().subscribe(
  			(data: any[]) => this.profiles = data,
  			error => console.log(error.error)
  		);
  	}

    delete(id) {
      this.http.delete(id).subscribe(
        data  => this.getList(),
        error => this.handleError(error)
    );
  }
  
  handleError(error) {
    console.log(error);
  }
}
