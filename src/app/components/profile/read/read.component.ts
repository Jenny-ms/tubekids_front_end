import { Component, OnInit } from '@angular/core';

import { ActivatedRoute} from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpProfilesService } from '../../../Services/http-profiles.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.css']
})
export class ProfileReadComponent implements OnInit {
	public profile : any;
	private url : string;
	private id: number;

 	constructor(
 		private http   : HttpProfilesService,
		private route  : ActivatedRoute,
		private router : Router,
  	) {
  		this.profilebyId();
		this.getProfile(); 
  	}

	ngOnInit() {
		
	}

  	profilebyId() {
		this.route.params.subscribe(params => {
	        if(params['id']!=null){
	        	this.id  = params['id'];
	        }
	    });
	}

	getProfile() {
		this.http.getprofile(this.id).subscribe(
  			(data: any)  => this.handleResponse(data),
			error => this.handleError(error)
  		);
	}
	handleResponse(data) {
		this.profile = data.data;
	}

	handleError(error) {
		console.log(error);
	}

}
