import { Component, OnInit } from '@angular/core';

import { ActivatedRoute} from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpProfilesService } from '../../../Services/http-profiles.service';
import { Router } from '@angular/router';
import { TokenService } from '../../../services/token.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class ProfileUpdateComponent implements OnInit {

  
  ngOnInit() {
  }
  public profile : any;
  private url : string;
  private id: number;

  public error = [];

  constructor(
  	private http: HttpProfilesService,
  	private Token : TokenService,
  	private router: Router,
  	private route: ActivatedRoute,
  	) 
  {
  	this.profilebyId();
	this.getProfile();
}

	profilebyId() {
		this.route.params.subscribe(params => {
	        if(params['id']!=null){
	        	this.id  = params['id'];
	        }
	    });
	}
	getProfile() {
		this.http.getprofile(this.id).subscribe(
  			(data: any)  => this.handleResponse(data),
			error => this.handleError(error)
  		);
	}
	onSubmit(){
	this.http.put(this.profile).subscribe(
  			data  => this.successResponse(data),
			error => this.errorResponse(error)
  		);
	}

  	handleResponse(data) {
		this.profile = data.data;
	}

	handleError(error) {
		console.log(error);
	}

	successResponse(data) {
		this.router.navigateByUrl('/profile');
	}

	errorResponse(error) {
		this.error = error.error.errors;
	}

}
