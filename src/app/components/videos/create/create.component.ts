import { Component, OnInit } from '@angular/core';
import { HttpVideosService } from '../../../Services/http-videos.service';
import { TokenService } from '../../../services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class VideoCreateComponent implements OnInit {
	public form : FormData;
	public error = [];

	public video = {
		url : null, 
		name: null,
		type: null,
	};
  constructor(private http: HttpVideosService,
  	private Token : TokenService,
  	private router: Router) {
  	this.form = new FormData();
  }
  ngOnInit() {
  }

  onSubmit(){
  	if (this.form.has('name')) {
		this.http.post(this.form).subscribe(
			data => this.handleResponse(data),
			error => this.handleError(error)
		);
  	} else {
  		this.http.post(this.video).subscribe(
			data => this.handleResponse(data),
			error => this.handleError(error)
		);
  	}
}

handleResponse(data){
	this.router.navigateByUrl('/videos');
} 
handleError(error) {
	this.error = error.error.errors;
}

  onChange(event){
  	let video: FileList = event.target.files;
  	if(video.length > 0) {
  	  let file: File = video[0];
  	  this.form.append('video', file, file.name);
  	}
  }
}
