import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { HttpVideosService } from '../../../Services/http-videos.service';
import { ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class VideoMainComponent implements OnInit {

  	public videos : any[]
	  private url : string;

  	constructor(private http : HttpVideosService, private route : ActivatedRoute,
    private router : Router,
    private sanitizer: DomSanitizer) {
  		this.url = "videos";
  	}

  	ngOnInit() {
  		this.getList();
  	}

    path(url) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

  	getList() {
  		this.http.get().subscribe(
  			(data: any[]) => this.videos = data,
  			error => console.log(error.error)
  		);
  	}

    delete(id) {
      this.http.delete(id).subscribe(
        data  => this.getList(),
        error => this.handleError(error)
    );
  }
  
  handleError(error) {
    console.log(error);
  }

}
