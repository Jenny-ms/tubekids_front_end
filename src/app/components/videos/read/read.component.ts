import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpVideosService } from '../../../Services/http-videos.service';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.css']
})
export class VideoReadComponent implements OnInit {

  public videos : any;
	private url : string;
	private id: number;

 	constructor(
 		private http : HttpVideosService,
		private route       : ActivatedRoute,
		private router : Router,
		 private sanitizer: DomSanitizer
  	) {
  		this.videobyId();
		this.getVideo(); 
  	}

	ngOnInit() {
		
	}
	 path(url) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

  	videobyId() {
		this.route.params.subscribe(params => {
	        if(params['id']!=null){
	        	this.id  = params['id'];
	        }
	    });
	}

	getVideo() {
		this.http.getvideo(this.id).subscribe(
  			(data: any)  => this.handleResponse(data),
			error => this.handleError(error)
  		);
	}
	handleResponse(data) {
		this.videos = data.data;
	}

	handleError(error) {
		console.log(error);
	}

}
