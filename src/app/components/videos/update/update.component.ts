import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpVideosService } from '../../../Services/http-videos.service';
import { Router } from '@angular/router';
import { TokenService } from '../../../services/token.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class VideoUpdateComponent implements OnInit {


    public video     : any;
  	private id      : string;
  	public error    : any[];
  	public formData : FormData;

  	constructor(
  		private http   : HttpVideosService,
		private router : Router,
		private route  : ActivatedRoute,
  	) { 
    	this.error    = [];
    	this.formData = new FormData();
  	}

  	ngOnInit() {
		this.videoById();
 		this.getVideo();
	}

	videoById() {
		this.route.params.subscribe(params => {
	        if(params['id']!=null){
	        	this.id  = params['id'];
	        }
	    });
	}

	getVideo() {
		this.http.getvideo(this.id).subscribe(
  			(data: any)  => this.successResponse(data),
			error        => this.errorResponse(error)
  		);
	}

  	successResponse(data) {
		this.video = data.data;
	}

	errorResponse(error) {
		console.log(error);
	}

  	edit() {
	  	if (this.video.type == 'true') {
 	    	this.formData.append('url', this.video.url);
	  	}
    	this.formData.append('id', this.video.id.toString());
 	    this.formData.append('name', this.video.name);
 	    this.formData.append('type', this.video.type);
	 	this.formData.append('user_id', this.video.user_id.toString());

	  	this.http.put(this.formData).subscribe(
	  		data  => this.handleResponse(data),
			error => this.handleError(error)
	  	);
  	}

  	handleResponse(data) {
		this.router.navigateByUrl('/videos');
	}

	handleError(error) {
		this.error = error.error.errors;
	}

	onChange(event){
		let video: FileList = event.target.files;
	  	if(video.length > 0) {
	  	    let file: File = video[0];
	  	    this.formData.append('video', file, file.name);
	  	}
	}
}
